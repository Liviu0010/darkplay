A website that can host audio files and create audio libraries.

The libraries are created anonymously and they have three access levels:
->Public: Will appear on the list on the first page.
->Unlisted: Will not appear on the list, but can still be accessed using their name
  in the search function.
->Private: Similar to Unlisted, however a password is needed to access the library.

Every library, regardless of access level, will have a Mutation Password that is needed
in order to change different properties of the library (like library name, file/folder names, 
file/folder location, file/folder deletion and so on...).

Regarding development, so far the plan is to only use pure HTML, CSS, JS, PHP and SQL.
