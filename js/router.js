class Route {
    //name - name of the route
    //address - address (like /, /index, etc)
    //loader - callback to a function that loads the content
    constructor(name, address, loader) {
        this.name = name;
        this.address = address;
        this.load = loader;
    }
}

//contains the route loader functions
//basically what elements to load on each page
class RouteLoaders {
    static loadIndexPage() {
        let contentContainer = document.getElementById("contentContainer");
        let topMenuContainer = document.getElementById("topMenuContainer");

        topMenuContainer.appendChild(UIBuilder.createTopMenu());

        contentContainer.appendChild(UIBuilder.createSearchBar());
        contentContainer.appendChild(UIBuilder.createLibraryList());

        if(!Player.peekInstance()) {
            playerContainer.appendChild(UIBuilder.createPlayer());
            Player.getInstance();
        }
    }

    static loadLibraryPage(libraryName, folderStack) {
        let contentContainer = document.getElementById("contentContainer");
        let topMenuContainer = document.getElementById("topMenuContainer");
        let playerContainer = document.getElementById("playerContainer");

        topMenuContainer.appendChild(UIBuilder.createTopMenu());
        contentContainer.appendChild(UIBuilder.createSongList(libraryName, folderStack));
        
        if(!Player.peekInstance()) {
            playerContainer.appendChild(UIBuilder.createPlayer());
            Player.getInstance();
        }
    }
}

class Router {
    constructor() {
        
    }

    static getInstance() {
        if(!Router.instance) {
            Router.instance = new Router();
        }

        return Router.instance;
    }

    /*
    Returns a boolean value specifying whether we are within
    a library context or we're outside of a library.
    */
    libraryContext(path="") {
        if(path === "")
            return location.pathname.split("/")[1] == "library";
        else
            return path.split("/")[1] == "library";
    }

    loadCurrentPath() {
        let path = window.location.pathname;
        path = decodeURIComponent(path);

        this.clearPage();

        if(this.libraryContext()) {
            RouteLoaders.loadLibraryPage(path.split("/")[2], path.split("/").slice(3));
        }
        else
            RouteLoaders.loadIndexPage();
    }

    loadPath(path) {
        path = decodeURIComponent(path);
        //so it doesn't keep pushing the same link 20 times
        if(window.location.pathname+window.location.search != path)
                history.pushState({}, "", encodeURI(path));

        this.clearPage();

        if(this.libraryContext(path)) {
            //this is triggered from clicking on a link
            RouteLoaders.loadLibraryPage(path.split("/")[2], path.split("/").slice(3));
        }
        else 
        {
            path = path.split('?')[0]; //getting rid of any parameters
            RouteLoaders.loadIndexPage();
        }
    }

    clearPage() {
        let content = document.getElementById("contentContainer");
        let topMenuContainer = document.getElementById("topMenuContainer");
        let playerContainer = document.getElementById("playerContainer");

        if(topMenuContainer.firstChild)
            topMenuContainer.removeChild(topMenuContainer.firstChild);
        
        //if(playerContainer.firstChild)
         //   playerContainer.removeChild(playerContainer.firstChild);

        while(content.firstChild) {
            content.removeChild(content.firstChild);
        }
    }
}