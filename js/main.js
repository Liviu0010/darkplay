window.onload = function () {
    
    //whenever we go back (pop one item in the history stack), load that page
    window.onpopstate = () => Router.getInstance().loadCurrentPath();

    //loads the index page
    Router.getInstance().loadCurrentPath();
}