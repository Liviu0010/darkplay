//handles the audio player
class Player {
    constructor(){
        this.player = document.getElementById("player");
        this.songTitle = document.getElementById("songTitle");
    }

    static getInstance(){
        if(!this.instance){
            this.instance = new Player();
        }

        return this.instance;
    }

    static peekInstance() {
        return this.instance;
    }

    play(songListItem) {
        let a = songListItem.firstChild;
        this.player.src = a.href;
        this.songTitle.innerText = a.innerText;

        
    }
}