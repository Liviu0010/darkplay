//class that is used for creating the main UI elements
class UIBuilder {
    static createSearchBar() {
        let textInput = document.createElement("input");
        let searchButton = document.createElement("button");
        let container = document.createElement("div");

        textInput.type = "text";
        textInput.placeholder = "Library Name";
        searchButton.innerText = "Go";
        searchButton.onclick = function() {
            //TO DO:
            //do a GET request for the selected library
            console.log("wew looking for: " + textInput.value);
            //document.getElementById("libraryList").appendChild(ListItemBuilder.createLibraryListItem(textInput.value, "/"));
            //END
        };
        //I want the button click even to be trigger when
        //the user presses Enter in the text input field
        textInput.onkeydown = (e) => {
            if(e.key == "Enter") 
                searchButton.onclick();
        };

        container.id = "searchBarContainer";
        container.appendChild(textInput);
        container.appendChild(searchButton);

        return container;
    }

    static createTopMenu() {
        let container = document.createElement("div");

        container.appendChild(TopMenuButtonsBuilder.createTopMenuButton("/res/img/icon.png", "/", () => Router.getInstance().loadPath("/")));

        //only showing the "Add Library" button if we're not in a library
        if(!Router.getInstance().libraryContext())
            container.appendChild(TopMenuButtonsBuilder.createTopMenuButton("/res/img/add_icon.png", "#", 
                () => { let l = UIBuilder.createAddLibraryDialog();
                        document.body.appendChild(l);
                        l.childNodes[2].focus();    //focus on the name input
                }));

        return container;
    }

    static createLibraryList() {
        let ul = document.createElement("ul");
        ul.id = "libraryList";

        fetch("/php/responder.php?librarylist")
        .then(response => response.json())
        .then(data => {
            //it's actually an array of library names
            for(let i = 0; i < data.length; i++) {
                ul.appendChild(ListItemBuilder.createLibraryListItem(data[i], "/library/" + encodeURIComponent(data[i])));
            }
        });

        return ul;
    }

    static createSongList(libraryName, folderStack) {
        let ul = document.createElement("ul");
        ul.id = "songList";

        if(folderStack[0] == "")
            folderStack = folderStack.slice(1);

        if(folderStack.length > 0)
            folderStack = folderStack.reduce((left, right) => left+"/"+right);

        fetch("/php/responder.php?library&libname="+encodeURIComponent(libraryName) + "&folderstack=" + encodeURIComponent(folderStack))
        .then(response => response.json())
        .then(data => {
            for(let i = 0; i < data.length; i++)
                if(data[i][1] == false)
                    ul.appendChild(ListItemBuilder.createSongListItem(data[i][0], "/library/"+encodeURIComponent(libraryName)+'/'+encodeURIComponent(folderStack) + "/" + encodeURIComponent(data[i][0])));
                else
                    ul.appendChild(ListItemBuilder.createSongListItemDirectory(data[i][0], "/library/"+encodeURIComponent(libraryName)+'/'+encodeURIComponent(folderStack) + "/" + encodeURIComponent(data[i][0])));
        });

        return ul;
    }

    static createPlayer() {
        let player = document.createElement("audio");
        let songTitle = document.createElement("span");
        let volumeSlider = document.createElement("input");
        let controlsWrapper = document.createElement("div");
        let wrapper = document.createElement("div");

        player.id = "player";
        player.controls = true;
        player.autoplay = true;
	player.loop = true;
	
        songTitle.id = "songTitle";

        volumeSlider.type = "range";
        volumeSlider.min = 0;
        volumeSlider.max = 100;
        volumeSlider.value = player.volume * 100;

        volumeSlider.oninput = () => {player.volume = parseFloat(volumeSlider.value)/100; };

        controlsWrapper.appendChild(volumeSlider);
        controlsWrapper.appendChild(songTitle);
        wrapper.appendChild(controlsWrapper);
        wrapper.appendChild(player);

        return wrapper;
    }

    //creates a password input with reveal functionality
    //password input is the first child for easy access
    static createPasswordInput() {
        let container = document.createElement("div");
        let input = document.createElement("input");
        let revealPassword = document.createElement("button");

        input.type = "password";
        revealPassword.innerText = "👁";

        revealPassword.style.display = "inline";
        revealPassword.style.width = "auto";
        container.style.whiteSpace = "nowrap";

        revealPassword.onclick = () => {
            if(input.type == "password")
                input.type = "text";
            else
                input.type = "password";
        }

        container.appendChild(input);
        container.appendChild(revealPassword);

        return container;

    }

    static createAddLibraryDialog() {
        let container = document.createElement("div");
        let h2 = document.createElement("h2");
        let nameP = document.createElement("p");
        let accessLevelP = document.createElement("p");
        let mutationPasswordP = document.createElement("p");
        let accessPasswordP = document.createElement("p");
        let nameInput = document.createElement("input");
        let accessLevelDropdown = document.createElement("select");
        let mutationPasswordInput = UIBuilder.createPasswordInput();
        let accessPasswordInput = UIBuilder.createPasswordInput();
        let POSTButton = document.createElement("button");
        let cancelButton = document.createElement("button");
        let buttonContainer = document.createElement("div");
        let createOption = (value, text) => {
                let option = document.createElement("option");
                option.value = value;
                option.innerText = text;

                return option;
            };

        h2.innerText = "Add Library";
        nameP.innerText = "Library name";
        accessLevelP.innerText = "Access level";
        mutationPasswordP.innerText = "Mutation password";
        accessPasswordP.innerText = "Access password";

        accessLevelDropdown.add(createOption(0, "Public"));
        accessLevelDropdown.add(createOption(1, "Unlisted"));
        accessLevelDropdown.add(createOption(2, "Private"));

        container.id = "addLibraryDialog";

        POSTButton.innerText = "Add Library";
        POSTButton.onclick = () => {
            console.log("name = " + nameInput.value);
            console.log("accessLevel = " + accessLevelDropdown.options[accessLevelDropdown.selectedIndex].value);
            console.log("mutation pass = " + mutationPasswordInput.firstChild.value);
            console.log("access pass = " + accessPasswordInput.firstChild.value);

            let reqData = new FormData();
            reqData.set("type", "CREATE LIBRARY");
            reqData.set("data", JSON.stringify({
                name: nameInput.value,
                accessLevel: accessLevelDropdown.options[accessLevelDropdown.selectedIndex].value,
                mutationPassword: mutationPasswordInput.firstChild.value,
                accessPassword: accessPasswordInput.firstChild.value
            }));

            fetch("/php/libraryOperations.php", {
                method: "POST",
                headers: {
                    "Content" : "application/form-data"
                },
                body: reqData
            });

            document.body.removeChild(container);
        };

        cancelButton.innerText = "Cancel";
        cancelButton.onclick = () => {
            document.body.removeChild(container);
        }

        nameInput.type = "text";
        nameInput.name = "name";
        accessLevelDropdown.name = "accessLevel";
        mutationPasswordInput.firstChild.name = "mutationPassword";
        accessPasswordInput.firstChild.name = "accessPassword";

        nameInput.tabIndex = 1;
        accessLevelDropdown.tabIndex = 1;
        mutationPasswordInput.firstChild.tabIndex = 1;
        accessPasswordInput.firstChild.tabIndex = 1;
        POSTButton.tabIndex = 1;
        cancelButton.tabIndex = 1;

        buttonContainer.appendChild(POSTButton);
        buttonContainer.appendChild(cancelButton);

        container.appendChild(h2);
        container.appendChild(nameP);
        container.appendChild(nameInput);
        container.appendChild(accessLevelP);
        container.appendChild(accessLevelDropdown);
        container.appendChild(mutationPasswordP);
        container.appendChild(mutationPasswordInput);
        container.appendChild(accessPasswordP);
        container.appendChild(accessPasswordInput);
        container.appendChild(buttonContainer);

        return container;
    }
}

//class that is used for creating menu buttons
class TopMenuButtonsBuilder {
    //The new generic builder method is better
    /*
    static createIndexButton() {
        let anchor = document.createElement("a");
        let img = document.createElement("img");

        img.src = "/res/img/icon.png";
        anchor.appendChild(img);
        anchor.href = "/";
        anchor.onclick = (e) => {e.preventDefault(); Router.getInstance().loadPath("/");};

        return anchor;
    }
    */

    static createTopMenuButton(imgSource, anchorHref, onClickCallback) {
        let anchor = document.createElement("a");
        let img = document.createElement("img");

        img.src = imgSource;
        anchor.appendChild(img);
        anchor.href = anchorHref;
        anchor.onclick = (e) => {e.preventDefault(); onClickCallback();};

        return anchor;
    }
}

//class that is used for creating list items
class ListItemBuilder {

/*
    name - the text that will be displayed on the list item
    address - the url the user will be taken to when they click the item
*/
    static createLibraryListItem(name, address) {
        let item = document.createElement("li");
        let a = document.createElement("a");

        a.href = address;
        a.onclick = (e) => {e.preventDefault(); Router.getInstance().loadPath(address);}
        a.innerText = name;

        item.appendChild(a);

        return item;
    }

    static createSongListItem(name, address) {
        let item = document.createElement("li");
        let a = document.createElement("a");
        let extension = name.split(".")

        a.href = address;
        a.onclick = (e) => {e.preventDefault(); Player.getInstance().play(item);}
        
        a.innerText = name;

        item.appendChild(a);

        return item;
    }

    static createSongListItemDirectory(name, address) {
        let item = document.createElement("li");
        let a = document.createElement("a");
        let extension = name.split(".")

        a.href = address;
        a.onclick = (e) => {e.preventDefault(); Router.getInstance().loadPath(address);}
        
        a.innerText = name;

        item.appendChild(a);

        return item;
    }
}
