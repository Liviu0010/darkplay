<?php
//handles file/folder moving, renaming, deletion

require_once 'exceptionCodes.php';

class FileOperations {
    //returns an array of elements in the format [filename, isDirectory]
    static function getFileFolderList($path) { 
        //client trying to access directories it shouldn't
        if(strstr($path, "/../"))
            return [];
        $fullpath = __DIR__ . "/../library/" . $path;
        // script dir is /website/php, but the library dir is in /website/libraries
        $filesarr = scandir($fullpath);
        natsort($filesarr);
        unset($filesarr[0]);    //removes .
        unset($filesarr[1]);    //removes ..
        $filesarr = array_values($filesarr);

        $res = array();

        for($i = 0; $i < count($filesarr); $i++) {
            array_push($res, [$filesarr[$i], is_dir($fullpath . "/" . $filesarr[$i])]);
        }
        
        return $res;
    }

    //creates a folder at $path
    static function createFolder($path) {
        if(file_exists($path))
            throw new Exception("Directory already exists!", ExceptionCodes::FILE_OR_FOLDER_ALREADY_EXISTS);
        mkdir($path);
    }
}

?>