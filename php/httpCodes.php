<?php
//http codes that are used

class HTTPCodes {
    const OK = 200;
    const NOT_FOUND = 404;
    const UNAUTHORIZED = 401;
}

//getting constants on the client-side
if(isset($_GET['httpcodes']))
    echo(json_encode((new ReflectionClass('HTTPCodes'))->getConstants()));

?>