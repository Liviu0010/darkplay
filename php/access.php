<?php
//This class only exists to make the code more readable >_>

class Access {
    const READ_ACCESS = 0;
    const READ_WRITE_ACCESS = 1;

    static function hasReadAccess($code) {
        return $code >= Access::READ_ACCESS;
    }

    static function hasReadWriteAccess($code) {
        return $code >= Access::READ_WRITE_ACCESS;
    }
}
?>