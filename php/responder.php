<?php
//just used for retrieving the library list, files and searching for an
//unlisted or private library
session_start();
require_once 'database.php';
require_once 'httpCodes.php';
require_once 'exceptionCodes.php';
require_once 'access.php';
require_once 'library.php';
require_once 'fileOperations.php';

if(isset($_GET['librarylist'])) {
    echo(json_encode(Database::getInstance()->getPublicLibraries()));
}
else if(isset($_GET['library'])) {
    try {
        $library = Database::getInstance()->getLibrary($_GET['libname']);

        if($library->accessLevel == 2 && Access::hasReadAccess($_SESSION[$library->name])) {
            http_response_code(HTTPCodes::UNAUTHORIZED);
            echo(json_encode(['exceptionCode' => ExceptionCodes::PASSWORD_PROTECTED_LIBRARY,
                              'exceptionMessage' => "The library is private! Authorization required!"]));
            exit();
        }

        echo(json_encode(FileOperations::getFileFolderList($_GET['libname'] . '/' . $_GET['folderstack'])));
        
    }
    catch(Exception $e){
        http_response_code(HTTPCodes::NOT_FOUND);
        echo(json_encode(['exceptionCode' => $e->getCode(),
                          'exceptionMessage' => $e->getMessage()]));
    }
}
else if(isset($_POST['authorizeread'])) {
    try {
        $library = Database::getInstance()->getLibrary($_POST['libname']);
        
        if(password_verify($_POST['password'], $library->accessPassword)) {
            $_SESSION[$library->name] = Access::READ_ACCESS;
            http_response_code(HTTPCodes::OK);
            echo(json_encode(['message' => "Reading authorized!"]));
        }
        else {
            http_response_code(HTTPCodes::UNAUTHORIZED);
            echo(json_encode(['exceptionCode' => ExceptionCodes::WRONG_PASSWORD,
                              'exceptionMessage' => "Wrong password!"]));
            exit();
        }
    }
    catch(Exception $e) {
        http_response_code(HTTPCodes::NOT_FOUND);
        echo(json_encode(['exceptionCode' => $e->getCode(),
                          'exceptionMessage' => $e->getMessage()]));
    }
}
else if(isset($_POST['authorizewrite'])) {
    try {
        $library = Database::getInstance()->getLibrary($_POST['libname']);
        
        if(password_verify($_POST['password'], $library->mutationPassword)) {
            $_SESSION[$library->name] = Access::READ_WRITE_ACCESS;
            http_response_code(HTTPCodes::OK);
            echo(json_encode(['message' => "Writing authorized!"]));
        }
        else {
            http_response_code(HTTPCodes::UNAUTHORIZED);
            echo(json_encode(['exceptionCode' => ExceptionCodes::WRONG_PASSWORD,
                              'exceptionMessage' => "Wrong password!"]));
            exit();
        }
    }
    catch(Exception $e) {
        http_response_code(HTTPCodes::NOT_FOUND);
        echo(json_encode(['exceptionCode' => $e->getCode(),
                          'exceptionMessage' => $e->getMessage()]));
    }
}

?>