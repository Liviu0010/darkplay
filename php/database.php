<?php
require_once 'exceptionCodes.php';
require_once 'library.php';

//handles all the database interactions
class Database {
    private $host = 'localhost';
    private $db = 'DarkPlay';
    private $user = 'DarkPlay';
    private $password = 'darkplay';
    private $charset = 'utf8';
    private $opt = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_EMULATE_PREPARES => false];
    private $pdo;
    private static $instance;

    private function __construct() {
        $dsn = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";
        $this->pdo = new PDO($dsn, $this->user, $this->password, $this->opt);
    }

    public function getInstance() {
        if(Database::$instance == NULL) {
            Database::$instance = new Database;
        }

        return Database::$instance;
    }

    /*
    Returns an array of strings representing the names of
    the public libraries.
    */
    public function getPublicLibraries() {
        $stmt = $this->pdo->prepare("SELECT name FROM Libraries");
        $stmt->execute();
        $results = array();

        for($i = 0; $i < $stmt->rowCount(); $i++) {
            array_push($results, $stmt->fetchColumn());
        }

        return $results;
    }

    /*
    Returns a Library object from the database.
    !!!The passwords in the library elements are hashed
    */
    public function getLibrary($name) {
        $stmt = $this->pdo->prepare("SELECT * FROM Libraries WHERE name = :name");
        $stmt->execute(['name' => $name]);

        if($stmt->rowCount() == 0) {
            throw new Exception("NOT FOUND", ExceptionCodes::NOT_FOUND);
        }
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $library = new Library($row['name'], $row['accessLevel'], 
                $row['mutationPassword'], $row['accessPassword']);
        
        return $library;
    }

    /*
    Replaces an old library in the database with the new
    Library object.
    name - name of the library to be replaced (name column in the DB)
    libraryObject - a Library object containing the information to be
                    added in the DB. (passwords must already be hashed)
    */
    public function updateLibrary($name, $libraryObject) {
        $stmt = $this->pdo
            ->prepare("UPDATE Libraries 
            SET name = :newname, accessLevel = :accessLevel, mutationPassword = :mutationPassword, accessPassword = :accessPassword
            WHERE name = :oldname");
        $stmt->execute([
            'oldname' => $name,
            'newname' => $libraryObject->name,
            'accessLevel' => $libraryObject->accessLevel,
            'mutationPassword' => $libraryObject->mutationPassword,
            'accessPassword' => $libraryObject->accessPassword
        ]);
    }

    /*
    Adds a library to the database.
    libraryObject - A Library object containing the information
                    that will be added into the database.
                    (passwords are not hashed)
    */
    public function addLibrary($libraryObject) {
        $stmt = $this->pdo->prepare("SELECT * FROM Libraries WHERE name = :name");
        $stmt->execute(['name' => $libraryObject->name]);
        $n = $stmt->fetchColumn();

        if($n) {
            throw new Exception("Library already exists!", ExceptionCodes::DUPLICATE_NAME);
        }

        $libraryObject->mutationPassword = password_hash($libraryObject->mutationPassword, PASSWORD_DEFAULT);
        $libraryObject->accessPassword = password_hash($libraryObject->accessPassword, PASSWORD_DEFAULT);

        $stmt = $this->pdo->prepare("INSERT INTO Libraries(name, accessLevel, mutationPassword, accessPassword) VALUES(:name, :accessLevel, :mutationPassword, :accessPassword)");
        $stmt->execute([
            'name' => $libraryObject->name,
            'accessLevel' => $libraryObject->accessLevel,
            'mutationPassword' => $libraryObject->mutationPassword,
            'accessPassword' => $libraryObject->accessPassword
        ]);
    }

    /*
    Deletes a library from the database.
    name - the name of the library to be deleted
    */
    public function removeLibrary($name) {
        $stmt = $this->pdo->prepare("DELETE FROM Libraries WHERE name = :name");
        $stmt->execute(['name' => $name]);
    }
}
?>
