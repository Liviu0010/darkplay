<?php
//defines the library object

class Library {
    public $name;
    public $accessLevel;
    public $mutationPassword;
    public $accessPassword;

    public function __construct($name, $accessLevel, $mutationPassword, $accessPassword) {
        $this->name = $name;
        $this->accessLevel = $accessLevel;
        $this->mutationPassword = $mutationPassword;
        $this->accessPassword = $accessPassword;
    }
}
?>