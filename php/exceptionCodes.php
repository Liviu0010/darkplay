<?php
//contains exception codes

class ExceptionCodes {
    const NOT_FOUND = 0;
    const PASSWORD_PROTECTED_LIBRARY = 1;
    const WRONG_PASSWORD = 2;
    const DUPLICATE_NAME = 3;
    const FILE_OR_FOLDER_ALREADY_EXISTS = 4;
}

//getting constants on the client-side
if(isset($_GET['exceptioncodes']))
    echo(json_encode((new ReflectionClass('ExceptionCodes'))->getConstants()));
?>