<?php
//used for library creation, deletion and modification
require_once "database.php";
require_once "library.php";
require_once "fileOperations.php";

if($_POST['type'] == "CREATE LIBRARY") {
    $data = json_decode($_POST['data']);

    try{
        if(strpos($data->name, "../") != false) {
            http_response_code(403);
            echo("EY DON'T");
            exit();
        }

        if($data->name == "" || $data->mutationPassword == "") {
            http_response_code(400);
            echo("Library name and mutation password are mandatory!");
            exit();
        }

        FileOperations::createFolder('../library/' . $data->name);
    }
    catch(Exception $e) {
        http_response_code(409);
        echo($e->getCode() . ':' . $e->getMessage());
        exit();
    }

    $libObj = new Library($data->name, $data->accessLevel, $data->mutationPassword, $data->accessPassword);
    Database::getInstance()->addLibrary($libObj);
    http_response_code(201);
}

?>